package Main;

import Main.Consensus.DataController;
import Main.Execution.QueryController;
import Main.Propagation.CommunicationController;

import java.io.File;

/**
 *
 */
public class Peer implements HugschainDB{
    //
    private DataController dataController;
    //
    private QueryController queryController;
    //
    private CommunicationController communicationController;
    //
    private final String PATH = "C:" + File.separator + "Users" + File.separator  + "b52368" + File.separator + "Downloads" + File.separator + "TEST";

    /**
     *
     */
    public Peer(){
        this.dataController = new DataController(PATH);
        this.queryController = new QueryController(this);
        this.communicationController = new CommunicationController(this);
    }

    /**
     *
     * @return
     */
    public DataController getDataController() {
        return dataController;
    }

    /**
     *
     * @param dataController
     */
    public void setDataController(DataController dataController) {
        this.dataController = dataController;
    }

    /**
     *
     * @return
     */
    public QueryController getQueryController() {
        return queryController;
    }

    /**
     *
     * @param queryController
     */
    public void setQueryController(QueryController queryController) {
        this.queryController = queryController;
    }

    /**
     *
     * @return
     */
    public CommunicationController getCommunicationController() {
        return communicationController;
    }

    /**
     *
     * @param communicationController
     */
    public void setCommunicationController(CommunicationController communicationController) {
        this.communicationController = communicationController;
    }

    @Override
    public boolean start() {
        return false;
    }

    @Override
    public boolean shutdown() {
        return false;
    }

    @Override
    public boolean clear() {
        return false;
    }

    @Override
    public boolean isolate() {
        return false;
    }

    @Override
    public boolean connect(String ip) {
        return false;
    }

    @Override
    public void processQuery(String query) {
        this.queryController.processQuery(query);
    }
}
