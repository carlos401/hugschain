package Main.Application;

import Main.HugschainDB;
import Main.Peer;
import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 *
 */
public class Application {
    //
    private HugschainDB hugschainDB;

    //
    private BufferedReader in;

    /**
     *
     */
    public Application(){
        this.hugschainDB = new Peer();
    }

    /**
     *
     */
    public void start(){
        try{
            this.in = new BufferedReader(new InputStreamReader(System.in));
            String inputLine;
            System.out.printf("Query:: ");
            while ((inputLine = in.readLine()) != null) {
                if (inputLine.equals("Bye.")){
                    break;
                } else {
                    hugschainDB.processQuery(inputLine);
                }
                System.out.printf("Query:: ");
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     *
     */
    public void forceStop(){
        System.exit(0);
    }
}