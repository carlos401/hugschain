package Main.Propagation;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Listener extends Thread {

    private final int PORT = 9476;

    private CommunicationController communicationController;

    private ServerSocket serverSocket;

    //
    private boolean state = false;

    /**
     * @param communicationController
     */
    public Listener(CommunicationController communicationController) {
        this.communicationController = communicationController;
        try {
            this.serverSocket = new ServerSocket(PORT);
            System.out.println(">> Escuchando en el puerto " + PORT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        try {
            Socket clientSocket = serverSocket.accept(); //accepts connection
            System.out.println("\n>> Se ha conectado un nuevo nodo al cluster: "
                    + clientSocket.getRemoteSocketAddress().toString());
            System.out.println("Query:: ");
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            this.communicate(out, in);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void communicate(PrintWriter out, BufferedReader in) {
        String inputLine, outputLine;
        outputLine = "READY";
        out.println(outputLine);

        try {
            while ((inputLine = in.readLine()) != null) {
                if (inputLine.equals("DISCONNECT"))
                    break;
                outputLine = this.processInput(inputLine);
                out.println(outputLine);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private String processInput(String inputLine) {
        if(inputLine!=null){
            switch (inputLine) {
                case "CONNECT":
                    return this.communicationController.getAllData();
                case "INSERTING":
                    return this.communicationController.stopLocalInserting();
                case "INSERTED":
                    return this.communicationController.restartLocalInserting();
                case "RECOVERY":
                    return this.communicationController.getAllData();
                default:
                    this.communicationController.addMinedBlock(inputLine);
                    break;
            }
        }
        return null;
    }
}
