package Main.Propagation;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 *
 */
public class Speaker extends Thread {

    private String hostName;
    private int portNumber;
    private CommunicationController communicationController;

    /**
     *
     * @param hostName
     * @param portNumber
     * @param communicationController
     */
    public Speaker(String hostName, int portNumber, CommunicationController communicationController){
        this.communicationController = communicationController;
        this.hostName = hostName;
        this.portNumber = portNumber;
    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        try {
            Socket clientSocket = new Socket(hostName, portNumber); //accepts connection
            System.out.println("\n>> Se ha conectado como un nuevo nodo al cluster: "
                    + clientSocket.getRemoteSocketAddress().toString());
            System.out.println("Query:: ");
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            this.communicate(out, in);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // in order to improve the protocol
    private String lastCommand;

    /**
     *
     * @param out
     * @param in
     */
    private void communicate(PrintWriter out, BufferedReader in) {
        String inputLine, outputLine;
        outputLine = "CONNECT";
        out.println(outputLine);
        this.lastCommand = "CONNECT";
        try {
            while ((inputLine = in.readLine()) != null) {
                if (inputLine.equals("DISCONNECT"))
                    break;
                outputLine = this.processInput(inputLine);
                out.println(outputLine);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     *
     * @param inputLine
     * @return
     */
    private String processInput(String inputLine) {
        if(inputLine!=null){
            switch (inputLine) {
                case "STOPED":
                    //proceder a insertar
                    //INSERTED
                    //
                    return this.communicationController.getAllData();
                case "SEND NEW BLOCK":
                    //enviar los datos del nuevo bloque
                    break;
                default:
                    if(this.lastCommand.equals("CONNECT")){
                        this.communicationController.setLocalDB(inputLine);
                    }
                    //this.communicationController.addMinedBlock(inputLine);
                    break;
            }
        }
        return null;
    }
}
