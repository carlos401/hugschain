package Main.Propagation;

import Main.Consensus.Block;
import Main.Consensus.StringUtil;
import Main.Peer;

import java.util.ArrayList;

/**
 *
 */
public class CommunicationController {
    //
    private Peer peer;

    private Listener listenerThread;

    private Speaker speakerThread;

    /**
     *
     * @param peer
     */
    public CommunicationController(Peer peer){
        this.peer = peer;
        this.listenerThread = new Listener(this);
        listenerThread.start();
    }

    /**
     *
     */
    public void connect(String hostname, int port){
        this.speakerThread = new Speaker(hostname,port,this);
        System.out.println("Speaker creado!");
    }

    public String getAllData(){
        ArrayList<String> data = new ArrayList<>(2);
        data.add(0,StringUtil.getJson(this.peer.getDataController().getBlockChain()));
        data.add(1,StringUtil.getJson(this.peer.getDataController().getMetadata()));
        return StringUtil.getJson(data);
    }

    public String stopLocalInserting(){
        this.peer.getDataController().setCanInsert(false); //stops local inserting
        return "STOPED";
    }

    public String restartLocalInserting(){
        this.peer.getDataController().setCanInsert(true); //stops local inserting
        return "SEND NEW BLOCK";
    }

    public void addMinedBlock(String input){
        Block block = StringUtil.getBlock(input);
        this.peer.getDataController().getBlockChain().addMinedBlock(block);
    }

    public void setLocalDB(String JSONdata){
        System.out.println("\n>> Sincronizando datos con el cluster...");
        ArrayList<String> data = StringUtil.getArrayList(JSONdata);
        this.peer.getDataController().setBlockChain(
                StringUtil.getBlockchain(data.get(0))
        );
        this.peer.getDataController().setMetadata(
                StringUtil.getArrayList(data.get(1))
        );
        System.out.println("\n>> Datos sincronizados correctamente");
    }

}
