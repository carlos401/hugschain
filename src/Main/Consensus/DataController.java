package Main.Consensus;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 */
public class DataController {
    //
    private BlockChain blockChain;

    //
    private Storable fileManager;

    //
    private ArrayList<String> metadata;

    //used by communication controller
    private boolean canInsert;

    /**
     *
     */
    public DataController(String mainPath) {
        this.canInsert = true;
        this.fileManager = new JSONFileManager(mainPath);
        this.blockChain = new BlockChain(3);
        try{
            System.out.println(">> Intentando cargar la Base de Datos...");
            File dataFile = new File(mainPath+ File.separator  + "data.json");
            File metadataFile = new File(mainPath+ File.separator  +"metadata.json");
            // load metadata
            if (metadataFile.exists() && !metadataFile.isDirectory()){
                String JSONmetadata = this.fileManager.readData("metadata.json");
                this.metadata = StringUtil.getArrayList(JSONmetadata);
                System.out.println("    >> Se cargaron metadatos en la Base de Datos");
                // load data
                if (dataFile.exists() && !dataFile.isDirectory()){
                    String JSONdata = this.fileManager.readData("data.json");
                    ArrayList<Block> data =
                            StringUtil.getListBlock(JSONdata);
                    this.blockChain.setBlocks(data);
                    System.out.println("    >> Se cargaron datos en la Base de Datos");
                    if(this.blockChain.isChainValid()){
                        System.out.println("        >> La cadena de bloques cargada es consistente");
                    } else {
                        System.out.println("        >> Error: La cadena de bloques cargada es inconsistente");
                        System.out.println("            >> Creando una nueva cadena...");
                        this.blockChain = new BlockChain(3);
                        System.out.println("        >> Nueva cadena creada de forma exitosa!");
                    }
                } else{
                    System.out.println("    >> No se cargaron datos en la Base de datos");
                }
            } else {
                System.out.println("    >> No se encontraron metadatos, iniciando una nueva Base de Datos...");
                this.metadata = new ArrayList<>();
                System.out.println("    >> Base de datos creada exitosamente");
            }
            System.out.println(">> Base de Datos cargada");
        } catch (Exception e){
            e.printStackTrace();
            System.out.println(">> Error: La creacion del controlador de datos ha fallado");
        }
    }

    /**
     *
     * @return
     */
    public boolean saveData(){
        try{
            this.fileManager.writeData(
                    StringUtil.getJson(this.blockChain.getAllBlocks()),"data.json");
            this.fileManager.writeData(
                    StringUtil.getJson(this.metadata),"metadata.json");
            return true;
        } catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    /**
     *
     * @param attributtes
     * @param key
     * @param value
     * @return
     */
    // SELECT ATT1, ATT2, ATT3 WHERE KEY = VALUE;
    public String processSpecificSearch(ArrayList<String> attributtes , String key, String value){
        ArrayList<Block> blocks = this.blockChain.getBlocks(key,value);
        //for answer
        ArrayList<HashMap<String,String>> dataObject = new ArrayList<>();
        for (Block block: blocks
                             ) {
            HashMap<String,String> blockData = block.getData();
            HashMap<String,String> answerData = new HashMap<>();
            for (String attributte: attributtes
                 ) {
                answerData.put(attributte,blockData.get(attributte));
            }
            dataObject.add(answerData);
        }
        return StringUtil.getJson(dataObject);
    }

    /**
     *
     * @param key
     * @param value
     * @return
     */
    // SELECT ALL WHERE KEY = VALUE;
    public String processSpecificSearch(String key, String value){
        ArrayList<Block> blocks = this.blockChain.getBlocks(key,value);
        //for answer
        ArrayList<HashMap<String,String>> dataObject = new ArrayList<>();
        for (Block block: blocks
             ) {
            dataObject.add(block.getData());
        }
        return StringUtil.getJson(dataObject);
    }

    /**
     *
     * @return
     */
    //SELECT ALL WHERE *;
    public String processFullSearch(){
        ArrayList<Block> blocks = this.blockChain.getAllBlocks();
        //for answer
        ArrayList<HashMap<String,String>> dataObject = new ArrayList<>();
        for (Block block: blocks
        ) {
            dataObject.add(block.getData());
        }
        return StringUtil.getJson(dataObject);
    }

    /**
     *
     * @param attributtes
     * @return
     */
    // SELECT ATT1, ATT2, ATT3 WHERE *;
    public String processSpecificSearch(ArrayList<String> attributtes){
        ArrayList<Block> blocks = this.blockChain.getAllBlocks();
        //for answer
        ArrayList<HashMap<String,String>> dataObject = new ArrayList<>();
        for (Block block: blocks
        ) {
            HashMap<String,String> blockData = block.getData();
            HashMap<String,String> answerData = new HashMap<>();
            for (String attributte: attributtes
            ) {
                answerData.put(attributte,blockData.get(attributte));
            }
            dataObject.add(answerData);
        }
        return StringUtil.getJson(dataObject);
    }

    /**
     *
     * @param fileName
     * @return
     */
    //INSERT document_name.json
    public boolean insertFromFile(String fileName){
        try{
            System.out.println(">> Insertando desde archivo...");
            HashMap<String,String> blockData =
                    StringUtil.getHashMap(this.fileManager.readData(fileName));
            Block lastBlock = this.blockChain.getLastBlock();
            if (lastBlock!=null){
                this.blockChain.addBlock(
                        new Block(blockData,lastBlock.getHash()));
            } else { //genesis block
                this.blockChain.addBlock(
                        new Block(blockData,"NullBecauseThisIsTheGenesisBlock"));
            }
            this.saveData();
            System.out.println(">> El bloque se insertó exitosamente!");
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    /**
     *
     * @param values
     * @return
     */
    public boolean insertFromList(ArrayList<String> values){
        try{
            System.out.println(">> Insertando el nuevo bloque...");
            if(values.size()==metadata.size()){
                HashMap<String,String> blockData = new HashMap<>();
                for (int i=0; i<metadata.size(); i++){
                    blockData.put(metadata.get(i),values.get(i));
                }
                Block lastBlock = this.blockChain.getLastBlock();
                if (lastBlock!=null){
                    this.blockChain.addBlock(
                            new Block(blockData,lastBlock.getHash()));
                } else { //genesis block
                    this.blockChain.addBlock(
                            new Block(blockData,"NullBecauseThisIsTheGenesisBlock"));
                }
                this.saveData();
                System.out.println(">> El bloque se insertó exitosamente!");
                return true;
            } else {
                return false;
            }
        } catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    /**
     *
     * @param filename
     * @return
     */
    public boolean importMetadata(String filename){
        try {
            if(this.blockChain.getAllBlocks().isEmpty()){
                System.out.println(">> Cargando modelo desde el archivo especificado...");
                this.metadata =
                        StringUtil.getArrayList(this.fileManager.readData(filename));
                this.saveData();
                System.out.println(">> Modelo importado con exito!");
                return true;
            } else{
                System.out.println(">> Error: No puede alterar el modelo porque la base de datos no esta vacia");
                return false;
            }
        } catch (Exception e){
            e.printStackTrace();
            System.out.println(">> Error: No se pudo cargar el archivo especificado");
            return false;
        }
    }

    /**
     *
     * @return
     */
    //GET STATE;
    public boolean getState(){
        try{
            return this.blockChain.isChainValid();
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    /**
     *
     * @param attr
     * @return
     */
    public boolean checkAttribute(String attr){
        if(attr.equals("ALL")){
            return true;
        } else if(this.metadata!=null){
            return this.metadata.contains(attr);
        }
        return false;
    }

    /**
     *
     * @return
     */
    public ArrayList<String> getMetadata() {
        return metadata;
    }

    /**
     *
     * @param metadata
     */
    public void setMetadata(ArrayList<String> metadata) {
        this.metadata = metadata;
    }

    public boolean isCanInsert() {
        return canInsert;
    }

    public void setCanInsert(boolean canInsert) {
        this.canInsert = canInsert;
    }

    public BlockChain getBlockChain() {
        return blockChain;
    }

    public void setBlockChain(BlockChain blockChain) {
        this.blockChain = blockChain;
    }
}
