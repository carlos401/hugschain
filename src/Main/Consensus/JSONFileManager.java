package Main.Consensus;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;

/**
 *
 */
public class JSONFileManager implements Storable {

    //
    private String PATH;

    /**
     *
     * @param path
     */
    public JSONFileManager(String path) {
        this.PATH = path;
    }

    /**
     * @return
     */
    @Override
    public String readData(String fileName) {
        try{
            String data="";
            FileReader file = new FileReader(PATH+"/"+fileName);
            BufferedReader buffer = new BufferedReader(file);
            String line;
            while((line=buffer.readLine())!=null){
                data+=line+"\n";
            }
            buffer.close();
            return data;
        }catch (Exception e){
            e.printStackTrace();
            return "";
        }
    }

    /**
     * @param data
     * @return
     */
    @Override
    public boolean writeData(String data,String fileName) {
        try{
            FileWriter file = new FileWriter(PATH+"/"+fileName);
            file.write(data);
            file.flush();
            file.close();
            return true;
        } catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }
}
