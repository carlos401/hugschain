package Main.Consensus;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 */
public class StringUtil {
    //
    private static StringUtil ourInstance = new StringUtil();

    //
    public static StringUtil getInstance() {
        return ourInstance;
    }

    /**
     *
     */
    private StringUtil() {

    }

    /**
     * Applies Sha256 to a string and returns the result.
     * @param input
     * @return
     */
    public static String applySha256(String input){
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            //Applies sha256 to our input,
            byte[] hash = digest.digest(input.getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer(); // This will contain hash as hexidecimal
            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if(hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        }
        catch(Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     *
     * @param difficulty
     * @return
     */
    //Returns difficulty string target, to compare to hash. eg difficulty of 5 will return "00000"
    public static String getDificultyString(int difficulty) {
        return new String(new char[difficulty]).replace('\0', '0');
    }

    /**
     *
     * @param o
     * @return
     */
    //Short hand helper to turn Object into a json string
    public static String getJson(Object o) {
        return new GsonBuilder().setPrettyPrinting().create().toJson(o);
    }

    /**
     *
     * @param jsonFile
     * @return
     */
    public static HashMap<String,String> getHashMap(String jsonFile){
        Type type = new TypeToken<HashMap<String,String>>(){}.getType();
        return new Gson().fromJson(jsonFile,type);
    }

    /**
     *
     * @param jsonFile
     * @return
     */
    public static ArrayList<String> getArrayList(String jsonFile){
        Type type = new TypeToken<ArrayList<String>>(){}.getType();
        return new Gson().fromJson(jsonFile,type);
    }

    /**
     *
     * @param jsonFile
     * @return
     */
    public static ArrayList<Block> getListBlock(String jsonFile){
        Type type = new TypeToken<ArrayList<Block>>(){}.getType();
        return new Gson().fromJson(jsonFile,type);
    }

    /**
     *
     * @param value
     * @return
     */
    public static String cleanValue(String value){
        return value.substring(1,value.length()-1);
    }

    /**
     *
     * @param jsonFile
     * @return
     */
    public static Block getBlock(String jsonFile){
        Type type = new TypeToken<Block>(){}.getType();
        return new Gson().fromJson(jsonFile,type);
    }

    /**
     *
     * @param jsonFile
     * @return
     */
    public static BlockChain getBlockchain(String jsonFile){
        Type type = new TypeToken<BlockChain>(){}.getType();
        return new Gson().fromJson(jsonFile,type);
    }
}
