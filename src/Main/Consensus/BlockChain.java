package Main.Consensus;
import java.util.ArrayList;

/**
 * This class provides the logical structure for a blockchain
 * @version 1.0
 * @author Carlos Delgado
 */
public class BlockChain {
    // the real chain of blocks
    private  ArrayList<Block> blocks;

    // difficulty for mining
    private int difficulty;

    /**
     * Default constructor
     * @param difficulty for mining
     */
    public BlockChain(int difficulty){
        this.blocks = new ArrayList<>();
        this.difficulty = difficulty;
    }

    /**
     * Adds block to the chain
     * @param data block to be added
     * @return true if the adding process was successful,
     *          false otherwise.
     */
    public boolean addBlock(Block data){
        try{
            data.mineBlock(this.difficulty);
            this.blocks.add(data);
            return true;
        } catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    /**
     *
     * @param data
     * @return
     */
    public boolean addMinedBlock(Block data){
        try{
            this.blocks.add(data);
            return true;
        } catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    /**
     *
     * @return
     */
    public Block getLastBlock(){
        try{
            if(!this.blocks.isEmpty()){
                return this.blocks.get(blocks.size()-1);
            }
            return null;
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Returns the blockchain in JSON format
     * @return the blockchain in JSON format
     */
    public String toJSON(){
        try{
            return StringUtil.getJson(this.blocks);
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Returns all the blocks in the current chain
     * @return all data blocks in the chain
     */
    public ArrayList<Block> getAllBlocks(){
        try{
            return this.blocks;
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Returns a subset of blocks, given an specific condition
     * @param attr the key for search
     * @param value the value for search
     * @return the data block associated with that search, in JSON format
     */
    public ArrayList<Block> getBlocks(String attr, String value){
        try{
            ArrayList<Block> dataObject = new ArrayList<>();
            for (Block block: this.blocks
            ) {
                if( block!=null &&
                        (block.getData().get(attr).equals(value))){
                    dataObject.add(block);
                }
            }
            return dataObject;
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Verifies if the chain is valid
     * @return true if the chain is valid, false otherwise
     */
    public boolean isChainValid() {
        try{
            Block currentBlock;
            Block previousBlock;
            String hashTarget = new String(new char[difficulty]).replace('\0', '0');

            //loop through blockchain to check hashes:
            for(int i=1; i < blocks.size(); i++) {
                currentBlock = blocks.get(i);
                previousBlock = blocks.get(i-1);
                //compare registered hash and calculated hash:
                if(!currentBlock.getHash().equals(currentBlock.calculateHash()) ){
                    System.out.println("    >> Error: Current Hashes not equal");
                    return false;
                }
                //compare previous hash and registered previous hash
                if(!previousBlock.getHash().equals(currentBlock.getPrevHash()) ) {
                    System.out.println("    >> Error: Previous Hashes not equal");
                    return false;
                }
                //check if hash is solved
                if(!currentBlock.getHash().substring( 0, difficulty).equals(hashTarget)) {
                    System.out.println("    >> Error: This block hasn't been mined "
                                            + currentBlock.getHash());
                    return false;
                }
            }
            return true;
        } catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    /**
     *
     * @param blocks
     */
    public void setBlocks(ArrayList<Block> blocks) {
        this.blocks = blocks;
    }

    /**
     *
     * @return
     */
    public int getDifficulty() {
        return difficulty;
    }

    /**
     *
     * @param difficulty
     */
    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }
}
