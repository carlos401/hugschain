package Main.Consensus;
import java.util.Date;
import java.util.HashMap;

/**
 * This class is the minimal structure for a blockchain
 * @version 1.0
 * @author Carlos Delgado
 */
public class Block{
    //Hash of block
    private String hash;

    //Hash of previous block
    private String prevHash;

    //Timestamp during creation
    private long timeStamp;

    //Variable for mining the block
    private int nonce;

    //Structure for data
    // use the form: attribute , value
    // example: "book tittle" , "The little prince"
    private HashMap <String, String> data;

    /**
     * Default constructor
     * @param data data to save in the block
     * @param prevHash hash of the previous block in the chain
     */
    public Block(HashMap<String, String> data, String prevHash){
        this.data = data;
        this.hash = calculateHash();
        this.timeStamp = new Date().getTime();
        this.prevHash = prevHash;
    }

    /**
     * Computes the hash for the current block
     * @return the hash associated to this block
     */
    public String calculateHash(){
        return StringUtil.applySha256(this.prevHash +
                Long.toString(this.timeStamp) +
                Integer.toString(nonce)
        );
    }

    /**
     * Mines the current block
     * @param difficulty difficulty level for mining
     */
    public void mineBlock(int difficulty){
        System.out.println("    >> Minando bloque...");
        String target = StringUtil.getDificultyString(difficulty);
        while(!this.hash.substring(0,difficulty).equals(target)){
            nonce++;
            hash = calculateHash();
        }
        System.out.println("    >> Bloque minado exitosamente!!! : " + this.hash);
    }

    /**
     *
     * @return
     */
    public String getHash() {
        return hash;
    }

    /**
     *
     * @param hash
     */
    public void setHash(String hash) {
        this.hash = hash;
    }

    /**
     *
     * @return
     */
    public String getPrevHash() {
        return prevHash;
    }

    /**
     *
     * @param prevHash
     */
    public void setPrevHash(String prevHash) {
        this.prevHash = prevHash;
    }

    /**
     *
     * @return
     */
    public long getTimeStamp() {
        return timeStamp;
    }

    /**
     *
     * @param timeStamp
     */
    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    /**
     *
     * @return
     */
    public HashMap<String, String> getData() {
        return data;
    }

    /**
     *
     * @param data
     */
    public void setData(HashMap<String, String> data) {
        this.data = data;
    }
}
