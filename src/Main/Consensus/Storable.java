package Main.Consensus;

/**
 * This interface provides persistence
 * @author Carlos Delgado
 * @version 1.0
 *
 */
public interface Storable {
    /**
     *
     * @return
     */
    String readData(String fileName);

    /**
     *
     * @param data
     * @return
     */
    boolean writeData(String data, String fileName);
}
