package Main.Execution;

import Main.Execution.CocoR.Parser;
import Main.Execution.CocoR.Scanner;
import Main.Peer;
import java.io.ByteArrayInputStream;

/**
 *
 */
public class QueryController {

    //
    private Peer peer;

    /**
     *
     * @param peer
     */
    public QueryController(Peer peer){
        this.peer = peer;
    }

    /**
     *
     * @param query
     */
    public void processQuery(String query){
        Scanner scanner = new Scanner(new ByteArrayInputStream(query.getBytes()));
        Parser parser = new Parser(scanner,this.peer);
        parser.Parse();
    }

}
