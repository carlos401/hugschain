package Main.Execution.Parser;

public class Errors {
    public int count = 0;                                    // number of errors detected
    public java.io.PrintStream errorStream = System.out;     // error messages go to this stream
    public String errMsgFormat = "-- line {0} col {1}: {2}"; // 0=line, 1=column, 2=text

    protected void printMsg(int line, int column, String msg) {
        StringBuffer b = new StringBuffer(errMsgFormat);
        int pos = b.indexOf("{0}");
        if (pos >= 0) {
            b.delete(pos, pos + 3);
            b.insert(pos, line);
        }
        pos = b.indexOf("{1}");
        if (pos >= 0) {
            b.delete(pos, pos + 3);
            b.insert(pos, column);
        }
        pos = b.indexOf("{2}");
        if (pos >= 0) b.replace(pos, pos + 3, msg);
        errorStream.println(b.toString());
    }

    public void SynErr(int line, int col, int n) {
        String s;
        switch (n) {
            case 0:
                s = "EOF expected";
                break;
            case 1:
                s = "documentName expected";
                break;
            case 2:
                s = "address expected";
                break;
            case 3:
                s = "port expected";
                break;
            case 4:
                s = "attribute expected";
                break;
            case 5:
                s = "comparation expected";
                break;
            case 6:
                s = "value expected";
                break;
            case 7:
                s = "\";\" expected";
                break;
            case 8:
                s = "\"import\" expected";
                break;
            case 9:
                s = "\"model\" expected";
                break;
            case 10:
                s = "\"insert\" expected";
                break;
            case 11:
                s = "\"values\" expected";
                break;
            case 12:
                s = "\"(\" expected";
                break;
            case 13:
                s = "\",\" expected";
                break;
            case 14:
                s = "\")\" expected";
                break;
            case 15:
                s = "\"create\" expected";
                break;
            case 16:
                s = "\"database\" expected";
                break;
            case 17:
                s = "\"get\" expected";
                break;
            case 18:
                s = "\"state\" expected";
                break;
            case 19:
                s = "\"connect\" expected";
                break;
            case 20:
                s = "\"with\" expected";
                break;
            case 21:
                s = "\":\" expected";
                break;
            case 22:
                s = "\"disconnect\" expected";
                break;
            case 23:
                s = "\"select\" expected";
                break;
            case 24:
                s = "\"all\" expected";
                break;
            case 25:
                s = "\"where\" expected";
                break;
            case 26:
                s = "\"*\" expected";
                break;
            case 27:
                s = "??? expected";
                break;
            case 28:
                s = "invalid HugSQL";
                break;
            case 29:
                s = "invalid Insert";
                break;
            case 30:
                s = "invalid Select";
                break;
            case 31:
                s = "invalid Select";
                break;
            default:
                s = "error " + n;
                break;
        }
        printMsg(line, col, s);
        count++;
    }

    public void SemErr(int line, int col, String s) {
        printMsg(line, col, s);
        count++;
    }

    public void SemErr(String s) {
        errorStream.println(s);
        count++;
    }

    public void Warning(int line, int col, String s) {
        printMsg(line, col, s);
    }

    public void Warning(String s) {
        errorStream.println(s);
    }
} // Errors
