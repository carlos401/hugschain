package Main.Execution.Parser;

import Main.Execution.Scanner.Scanner;
import Main.Execution.Scanner.Token;
import Main.Peer;
import Main.Consensus.StringUtil;

import java.util.ArrayList;

// Replace "ExamSolution" for your compiler name


public class Parser {
    public static final int _EOF = 0;
    public static final int _documentName = 1;
    public static final int _address = 2;
    public static final int _port = 3;
    public static final int _attribute = 4;
    public static final int _comparation = 5;
    public static final int _value = 6;
    public static final int maxT = 27;

    static final boolean _T = true;
    static final boolean _x = false;
    static final int minErrDist = 2;

    public Token t;    // last recognized token
    public Token la;   // lookahead token
    int errDist = minErrDist;

    public Scanner scanner;
    public Errors errors;

    Peer peer;
    ArrayList<String> attributes;
    String address;
    int port;

    /**
     * Prints a message in console
     * >> use this method for debugging
     *
     * @param message msg to be printed
     */
    void print(String message) {
        System.out.println(message);
    }


    // ************* scanner specification section *************


    public Parser(Scanner scanner, Peer peer) {
        this.scanner = scanner;
        errors = new Errors();
        this.peer = peer;
    }

    void SynErr(int n) {
        if (errDist >= minErrDist) errors.SynErr(la.line, la.col, n);
        errDist = 0;
    }

    public void SemErr(String msg) {
        if (errDist >= minErrDist) errors.SemErr(t.line, t.col, msg);
        errDist = 0;
    }

    void Get() {
        for (; ; ) {
            t = la;
            la = scanner.Scan();
            if (la.kind <= maxT) {
                ++errDist;
                break;
            }

            la = t;
        }
    }

    void Expect(int n) {
        if (la.kind == n) Get();
        else {
            SynErr(n);
        }
    }

    boolean StartOf(int s) {
        return set[s][la.kind];
    }

    void ExpectWeak(int n, int follow) {
        if (la.kind == n) Get();
        else {
            SynErr(n);
            while (!StartOf(follow)) Get();
        }
    }

    boolean WeakSeparator(int n, int syFol, int repFol) {
        int kind = la.kind;
        if (kind == n) {
            Get();
            return true;
        } else if (StartOf(repFol)) return false;
        else {
            SynErr(n);
            while (!(set[syFol][kind] || set[repFol][kind] || set[0][kind])) {
                Get();
                kind = la.kind;
            }
            return StartOf(syFol);
        }
    }

    void HugSQL() {
        switch (la.kind) {
            case 8: {
                Import();
                break;
            }
            case 10: {
                Insert();
                break;
            }
            case 15: {
                Create();
                break;
            }
            case 17: {
                State();
                break;
            }
            case 19: {
                Connect();
                break;
            }
            case 22: {
                Disconnect();
                break;
            }
            case 23: {
                Select();
                break;
            }
            default:
                SynErr(28);
                break;
        }
        Expect(7);
    }

    void Import() {
        Expect(8);
        Expect(9);
        Expect(1);
        peer.getDataController().importMetadata(t.val);

    }

    void Insert() {
        Expect(10);
        if (la.kind == 1) {
            Get();
            if (!peer.getDataController().insertFromFile(t.val))
                System.err.println(">> Error: el archivo especificado es invalido");

        } else if (la.kind == 11) {
            Get();
            Expect(12);
            attributes = new ArrayList<>();

            Expect(6);
            attributes.add(StringUtil.cleanValue(t.val));

            while (la.kind == 13) {
                Get();
                Expect(6);
                attributes.add(StringUtil.cleanValue(t.val));

            }
            Expect(14);
            if (!peer.getDataController().insertFromList(attributes))
                System.err.println(">> Error: el numero de valores a insertar no coincide con el modelo");

        } else SynErr(29);
    }

    void Create() {
        Expect(15);
        Expect(16);
        Expect(4);
        print(">> Error: caracteristica no esta incluida en esta version");

    }

    void State() {
        Expect(17);
        Expect(18);
        if (peer.getDataController().getState()) {
            System.out.println(">> La cadena se encuentra en un estado valido");
        } else {
            System.err.println(">> La cadena se encuentra en un estado invalido");
        }

    }

    void Connect() {
        Expect(19);
        Expect(20);
        Expect(2);
        address = t.val;
        System.out.println(address);

        Expect(21);
        Expect(3);
        System.out.println(t.val);
        port = Integer.parseInt(t.val);
        this.peer.getCommunicationController().connect(address, port);

    }

    void Disconnect() {
        Expect(22);
        System.out.println(">> Error: caracterÃ­stica no esta incluida en esta versiÃ³n");

    }

    void Select() {
        Expect(23);
        attributes = new ArrayList<>();

        if (la.kind == 24) {
            Get();
            attributes.add(t.val);

        } else if (la.kind == 4) {
            Get();
            if (peer.getDataController().checkAttribute(t.val)) {
                attributes.add(t.val);
            } else {
                this.SemErr("Atributo \"" + t.val + "\" no definido en el modelo");
            }

            while (la.kind == 13) {
                Get();
                Expect(4);
                if (peer.getDataController().checkAttribute(t.val)) {
                    attributes.add(t.val);
                } else {
                    this.SemErr("Atributo \"" + t.val + "\" no definido en el modelo");
                }

            }
        } else SynErr(30);
        Expect(25);
        if (la.kind == 26) {
            Get();
            if (attributes.contains("ALL")) {
                System.out.println(peer.getDataController().processFullSearch());
            } else {
                System.out.println(peer.getDataController().processSpecificSearch(attributes));
            }

        } else if (la.kind == 4) {
            Get();
            String key = t.val;
            String value = scanner.Peek().val;
            scanner.ResetPeek();
            if (peer.getDataController().checkAttribute(key)) {
                if (!attributes.contains("ALL")) {
                    System.out.println(peer.getDataController().processSpecificSearch(attributes, key, value.substring(1, value.length() - 1)));
                } else {
                    System.out.println(peer.getDataController().processSpecificSearch(key, value.substring(1, value.length() - 1)));
                }
            } else {
                this.SemErr("Atributo \"" + key + "\" no definido en el modelo");
            }

            Expect(5);
            Expect(6);
        } else SynErr(31);
    }


    public void Parse() {
        la = new Token();
        la.val = "";
        Get();
        HugSQL();
        Expect(0);

    }

    private static final boolean[][] set = {
            {_T, _x, _x, _x, _x, _x, _x, _x, _x, _x, _x, _x, _x, _x, _x, _x, _x, _x, _x, _x, _x, _x, _x, _x, _x, _x, _x, _x, _x}

    };
} // end Parser
