package Main.Execution.Scanner;

import Main.Execution.Parser.FatalError;

import java.io.InputStream;
import java.util.Map;
import java.util.HashMap;

public class Scanner {
    static final char EOL = '\n';
    static final int eofSym = 0;
    static final int maxT = 27;
    static final int noSym = 27;
    char valCh;       // current input character (for token.val)

    public Buffer buffer; // scanner buffer

    Token t;           // current token
    int ch;            // current input character
    int pos;           // byte position of current character
    int charPos;       // position by unicode characters starting with 0
    int col;           // column number of current character
    int line;          // line number of current character
    int oldEols;       // EOLs that appeared in a comment;
    static final StartStates start; // maps initial token character to start state
    static final Map literals;      // maps literal strings to literal kinds

    Token tokens;      // list of tokens already peeked (first token is a dummy)
    Token pt;          // current peek token

    char[] tval = new char[16]; // token text used in NextToken(), dynamically enlarged
    int tlen;          // length of current token


    static {
        start = new StartStates();
        literals = new HashMap();
        for (int i = 97; i <= 122; ++i) start.set(i, 26);
        for (int i = 48; i <= 50; ++i) start.set(i, 27);
        for (int i = 51; i <= 57; ++i) start.set(i, 19);
        start.set(61, 23);
        start.set(34, 24);
        start.set(59, 30);
        start.set(40, 31);
        start.set(44, 32);
        start.set(41, 33);
        start.set(58, 34);
        start.set(42, 35);
        start.set(Buffer.EOF, -1);
        literals.put("import", new Integer(8));
        literals.put("model", new Integer(9));
        literals.put("insert", new Integer(10));
        literals.put("values", new Integer(11));
        literals.put("create", new Integer(15));
        literals.put("database", new Integer(16));
        literals.put("get", new Integer(17));
        literals.put("state", new Integer(18));
        literals.put("connect", new Integer(19));
        literals.put("with", new Integer(20));
        literals.put("disconnect", new Integer(22));
        literals.put("select", new Integer(23));
        literals.put("all", new Integer(24));
        literals.put("where", new Integer(25));

    }

    public Scanner(String fileName) {
        buffer = new Buffer(fileName);
        Init();
    }

    public Scanner(InputStream s) {
        buffer = new Buffer(s);
        Init();
    }

    void Init() {
        pos = -1;
        line = 1;
        col = 0;
        charPos = -1;
        oldEols = 0;
        NextCh();
        if (ch == 0xEF) { // check optional byte order mark for UTF-8
            NextCh();
            int ch1 = ch;
            NextCh();
            int ch2 = ch;
            if (ch1 != 0xBB || ch2 != 0xBF) {
                throw new FatalError("Illegal byte order mark at start of file");
            }
            buffer = new UTF8Buffer(buffer);
            col = 0;
            charPos = -1;
            NextCh();
        }
        pt = tokens = new Token();  // first token is a dummy
    }

    void NextCh() {
        if (oldEols > 0) {
            ch = EOL;
            oldEols--;
        } else {
            pos = buffer.getPos();
            // buffer reads unicode chars, if UTF8 has been detected
            ch = buffer.Read();
            col++;
            charPos++;
            // replace isolated '\r' by '\n' in order to make
            // eol handling uniform across Windows, Unix and Mac
            if (ch == '\r' && buffer.Peek() != '\n') ch = EOL;
            if (ch == EOL) {
                line++;
                col = 0;
            }
        }
        if (ch != Buffer.EOF) {
            valCh = (char) ch;
            ch = Character.toLowerCase(ch);
        }

    }

    void AddCh() {
        if (tlen >= tval.length) {
            char[] newBuf = new char[2 * tval.length];
            System.arraycopy(tval, 0, newBuf, 0, tval.length);
            tval = newBuf;
        }
        if (ch != Buffer.EOF) {
            tval[tlen++] = valCh;

            NextCh();
        }

    }


    boolean Comment0() {
        int level = 1, pos0 = pos, line0 = line, col0 = col, charPos0 = charPos;
        NextCh();
        if (ch == '-') {
            NextCh();
            for (; ; ) {
                if (ch == 10) {
                    level--;
                    if (level == 0) {
                        oldEols = line - line0;
                        NextCh();
                        return true;
                    }
                    NextCh();
                } else if (ch == Buffer.EOF) return false;
                else NextCh();
            }
        } else {
            buffer.setPos(pos0);
            NextCh();
            line = line0;
            col = col0;
            charPos = charPos0;
        }
        return false;
    }


    void CheckLiteral() {
        String val = t.val;
        val = val.toLowerCase();

        Object kind = literals.get(val);
        if (kind != null) {
            t.kind = ((Integer) kind).intValue();
        }
    }

    Token NextToken() {
        while (ch == ' ' ||
                ch >= 9 && ch <= 10 || ch == 13
        ) NextCh();
        if (ch == '-' && Comment0()) return NextToken();
        int recKind = noSym;
        int recEnd = pos;
        t = new Token();
        t.pos = pos;
        t.col = col;
        t.line = line;
        t.charPos = charPos;
        int state = start.state(ch);
        tlen = 0;
        AddCh();

        loop:
        for (; ; ) {
            switch (state) {
                case -1: {
                    t.kind = eofSym;
                    break loop;
                } // NextCh already done
                case 0: {
                    if (recKind != noSym) {
                        tlen = recEnd - t.pos;
                        SetScannerBehindT();
                    }
                    t.kind = recKind;
                    break loop;
                } // NextCh already done
                case 1:
                    if (ch >= '0' && ch <= '9' || ch == '_' || ch >= 'a' && ch <= 'z') {
                        AddCh();
                        state = 1;
                        break;
                    } else if (ch == '.') {
                        AddCh();
                        state = 2;
                        break;
                    } else {
                        state = 0;
                        break;
                    }
                case 2:
                    if (ch == 'j') {
                        AddCh();
                        state = 3;
                        break;
                    } else {
                        state = 0;
                        break;
                    }
                case 3:
                    if (ch == 's') {
                        AddCh();
                        state = 4;
                        break;
                    } else {
                        state = 0;
                        break;
                    }
                case 4:
                    if (ch == 'o') {
                        AddCh();
                        state = 5;
                        break;
                    } else {
                        state = 0;
                        break;
                    }
                case 5:
                    if (ch == 'n') {
                        AddCh();
                        state = 6;
                        break;
                    } else {
                        state = 0;
                        break;
                    }
                case 6: {
                    t.kind = 1;
                    break loop;
                }
                case 7:
                    if (ch >= '0' && ch <= '2') {
                        AddCh();
                        state = 8;
                        break;
                    } else {
                        state = 0;
                        break;
                    }
                case 8:
                    if (ch >= '0' && ch <= '9') {
                        AddCh();
                        state = 9;
                        break;
                    } else {
                        state = 0;
                        break;
                    }
                case 9:
                    if (ch >= '0' && ch <= '9') {
                        AddCh();
                        state = 10;
                        break;
                    } else {
                        state = 0;
                        break;
                    }
                case 10:
                    if (ch == '.') {
                        AddCh();
                        state = 11;
                        break;
                    } else {
                        state = 0;
                        break;
                    }
                case 11:
                    if (ch >= '0' && ch <= '2') {
                        AddCh();
                        state = 12;
                        break;
                    } else {
                        state = 0;
                        break;
                    }
                case 12:
                    if (ch >= '0' && ch <= '9') {
                        AddCh();
                        state = 13;
                        break;
                    } else {
                        state = 0;
                        break;
                    }
                case 13:
                    if (ch >= '0' && ch <= '9') {
                        AddCh();
                        state = 14;
                        break;
                    } else {
                        state = 0;
                        break;
                    }
                case 14:
                    if (ch == '.') {
                        AddCh();
                        state = 15;
                        break;
                    } else {
                        state = 0;
                        break;
                    }
                case 15:
                    if (ch >= '0' && ch <= '2') {
                        AddCh();
                        state = 16;
                        break;
                    } else {
                        state = 0;
                        break;
                    }
                case 16:
                    if (ch >= '0' && ch <= '9') {
                        AddCh();
                        state = 17;
                        break;
                    } else {
                        state = 0;
                        break;
                    }
                case 17:
                    if (ch >= '0' && ch <= '9') {
                        AddCh();
                        state = 18;
                        break;
                    } else {
                        state = 0;
                        break;
                    }
                case 18: {
                    t.kind = 2;
                    break loop;
                }
                case 19:
                    if (ch >= '0' && ch <= '9') {
                        AddCh();
                        state = 20;
                        break;
                    } else {
                        state = 0;
                        break;
                    }
                case 20:
                    if (ch >= '0' && ch <= '9') {
                        AddCh();
                        state = 21;
                        break;
                    } else {
                        state = 0;
                        break;
                    }
                case 21:
                    if (ch >= '0' && ch <= '9') {
                        AddCh();
                        state = 22;
                        break;
                    } else {
                        state = 0;
                        break;
                    }
                case 22: {
                    t.kind = 3;
                    break loop;
                }
                case 23: {
                    t.kind = 5;
                    break loop;
                }
                case 24:
                    if (ch <= '!' || ch >= '#' && ch <= 65535) {
                        AddCh();
                        state = 24;
                        break;
                    } else if (ch == '"') {
                        AddCh();
                        state = 25;
                        break;
                    } else {
                        state = 0;
                        break;
                    }
                case 25: {
                    t.kind = 6;
                    break loop;
                }
                case 26:
                    recEnd = pos;
                    recKind = 4;
                    if (ch == '_' || ch >= 'a' && ch <= 'z') {
                        AddCh();
                        state = 26;
                        break;
                    } else if (ch >= '0' && ch <= '9') {
                        AddCh();
                        state = 1;
                        break;
                    } else if (ch == '.') {
                        AddCh();
                        state = 2;
                        break;
                    } else {
                        t.kind = 4;
                        t.val = new String(tval, 0, tlen);
                        CheckLiteral();
                        return t;
                    }
                case 27:
                    if (ch >= '0' && ch <= '9') {
                        AddCh();
                        state = 28;
                        break;
                    } else {
                        state = 0;
                        break;
                    }
                case 28:
                    if (ch >= '0' && ch <= '9') {
                        AddCh();
                        state = 29;
                        break;
                    } else {
                        state = 0;
                        break;
                    }
                case 29:
                    if (ch >= '0' && ch <= '9') {
                        AddCh();
                        state = 22;
                        break;
                    } else if (ch == '.') {
                        AddCh();
                        state = 7;
                        break;
                    } else {
                        state = 0;
                        break;
                    }
                case 30: {
                    t.kind = 7;
                    break loop;
                }
                case 31: {
                    t.kind = 12;
                    break loop;
                }
                case 32: {
                    t.kind = 13;
                    break loop;
                }
                case 33: {
                    t.kind = 14;
                    break loop;
                }
                case 34: {
                    t.kind = 21;
                    break loop;
                }
                case 35: {
                    t.kind = 26;
                    break loop;
                }

            }
        }
        t.val = new String(tval, 0, tlen);
        return t;
    }

    private void SetScannerBehindT() {
        buffer.setPos(t.pos);
        NextCh();
        line = t.line;
        col = t.col;
        charPos = t.charPos;
        for (int i = 0; i < tlen; i++) NextCh();
    }

    // get the next token (possibly a token already seen during peeking)
    public Token Scan() {
        if (tokens.next == null) {
            return NextToken();
        } else {
            pt = tokens = tokens.next;
            return tokens;
        }
    }

    // get the next token, ignore pragmas
    public Token Peek() {
        do {
            if (pt.next == null) {
                pt.next = NextToken();
            }
            pt = pt.next;
        } while (pt.kind > maxT); // skip pragmas

        return pt;
    }

    // make sure that peeking starts at current scan position
    public void ResetPeek() {
        pt = tokens;
    }

} // end Scanner
