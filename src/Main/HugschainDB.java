package Main;

public interface HugschainDB {

    boolean start();

    boolean shutdown();

    boolean clear();

    boolean isolate();

    boolean connect(String ip);

    void processQuery(String query);

}
